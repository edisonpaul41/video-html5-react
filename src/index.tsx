import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Route, Switch } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import VideoLayout from './modules/video/layouts/video.layout';


ReactDOM.render((
  <BrowserRouter>
    <Switch>
      <Route exact  path="/" component={VideoLayout} />
    </Switch>
  </BrowserRouter>


), document.getElementById('root'))
