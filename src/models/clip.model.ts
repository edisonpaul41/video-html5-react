export class Clip {
    start: number = 0;
    end: number = 52;
    url: string = '/assets/video.mp4';
    name: string = '';
    id: number = 0;
}

