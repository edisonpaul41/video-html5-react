import * as React from 'react';
import VideoStore from '../../store';
import { inject, observer } from 'mobx-react';
import { Col, Row, Container, Button, Label } from 'reactstrap';
import './video.container.css';
import ReproducerComponent from '../../components/reproducer/reproducer.component';
import AddEditVideoModal from '../../components/add-edit-modal/add-edit.modal';
class VideoContainerProps {
    store?: VideoStore;
}
@inject('store')
@observer
export default class VideoContainer extends React.Component<VideoContainerProps> {

    constructor(props: VideoContainerProps) {
        super(props);
        this.addClip = this.addClip.bind(this);
        this.setFocused = this.setFocused.bind(this);
        this.openModal = this.openModal.bind(this);
        this.deleteClip = this.deleteClip.bind(this);
        this.modifyClip = this.modifyClip.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.showCloseLoading = this.showCloseLoading.bind(this);
    }
    addClip() {
        this.props.store.addClip();
        this.props.store.openCloseModal(false);
    }
    modifyClip() {
        this.props.store.modifyClip();
        this.props.store.openCloseModal(false);
    }
    setFocused(id: number) {
        this.props.store.setFocusedVideo(id);
    }
    deleteClip(id: number) {
        this.props.store.deleteClip(id);
    }

    openModal(id?: number) {
        if (id) {
            this.props.store.setClipToEdit(JSON.parse(JSON.stringify(this.props.store.clips.find(cl => cl.id == id))));
        } else {
            let clip = {
                start: 0,
                end: 52,
                url: '/assets/video.mp4',
                name: '',
                id: 0
            }
            this.props.store.setClipToEdit(clip);
        }
        this.props.store.openCloseModal(true);
    }
    closeModal() {
        this.props.store.openCloseModal(false);
    }
    showCloseLoading(value: boolean){
        this.props.store.openCloseLoading(value);
        if(value){
            setTimeout(() => {
                if(this.props.store.nextVideoLoading){
                    this.props.store.getNextOnLine();
                    this.showCloseLoading(false);
                }
            }, 3000);
        }
    }
    cancelNextLoading(){
        this.props.store.openCloseLoading(false);
        this.props.store.allowAutoplay(false);
    }
    allowAutoPlay(){
        this.props.store.allowAutoplay(true);
    }


    render() {
        const { store } = this.props;
        let autoplay = store.autoplayNext ? null : (<Col md="12">
        <Button color="success main-video" onClick={()=> this.allowAutoPlay()}>Activate Autoplay</Button>
    </Col>);
        let video = this.props.store.nextVideoLoading ? (
            <Row className="loading">
                <Col md="12">
                    <Label>Loading next clip...</Label>
                </Col>
                <Col md="12">
                    <Button color="danger" onClick={()=> this.cancelNextLoading()}>Click to cancel</Button>
                </Col>
            </Row>
        ) : <Row>
                <Col md="12" className="App-header text-center">
                    <ReproducerComponent
                        showControl={true}
                        clip={store.clips.find(cl => cl.id == store.currentVideo)}
                        showCloseLoading={this.showCloseLoading}
                        focused= {true}
                        autoNext = {store.autoplayNext}
                    />
                </Col>
                {autoplay}
            </Row>;

        return (
            <Container>
                <Row className="main-row">
                    <Col md="7" className="main-video text-center">
                        {video}
                    </Col>
                    <Col md="1">
                        <div className="vl"></div>
                    </Col>
                    <Col md="4" className="list-videos text-center">
                        {store.clips.map((clip, index) => {
                            return (
                                <Row key={index}>
                                    <Col md="12" className="single-video text-center">
                                        <ReproducerComponent
                                            showControl={false}
                                            clip={clip}
                                            setDefault={this.setFocused}
                                            openModal={this.openModal}
                                            deleteClip={this.deleteClip}
                                        />
                                    </Col>
                                </Row>)
                        })}
                        <Row>
                            <Col className="text-center">
                                <Button className="btn-add" onClick={() => this.openModal()}>ADD CLIP</Button>
                            </Col>
                        </Row>
                    </Col>
                </Row>

                <AddEditVideoModal
                    showModal={store.modalOpen}
                    clip={store.videoFocused}
                    addClip={this.addClip}
                    editClip={this.modifyClip}
                    closeModal={this.closeModal}
                    attrModify={store.modifyAttributes}
                />
            </Container>
        );
    }
}

