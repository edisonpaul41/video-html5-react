import { observable, action } from 'mobx';
import { Clip } from '../../models/clip.model';


export default class VideoStore {
    @observable clips: Clip[] = [{
        name: 'Principal',
        start: 0,
        end: 52,
        url: '/assets/video.mp4',
        id: 1

    }];
    @observable videoFocused: Clip;
    @observable modalOpen: boolean = false;
    @observable currentVideo: number = 1;
    @observable nextVideoLoading: boolean = false;
    @observable autoplayNext: boolean = true;
    @action
    addClip = () => {
        let id = this.clips[this.clips.length - 1].id + 1;
        this.videoFocused.id = id;
        this.clips.push(this.videoFocused);
        this.videoFocused = undefined;
    }
    @action
    setFocusedVideo = (id: number) => {
        this.currentVideo = id;
    }
    @action
    setClipToEdit = (clip: Clip) => {
        this.videoFocused = clip;
    }
    @action
    openCloseModal = (value: boolean) => {
        this.modalOpen = value;
    }
    @action
    deleteClip = (id: number) => {
        this.currentVideo = 1;
        this.clips = this.clips.filter(cl => cl.id != id);
    }
    @action
    getNextOnLine = () => {
        let nextVideo = this.clips.filter(vid => vid.id > this.currentVideo)[0];
        if (nextVideo) {
            this.currentVideo = nextVideo.id;
        } else {
            this.currentVideo = 1;
            this.autoplayNext = false;
        }
    }
    @action
    modifyClip = () => {
        this.clips = this.clips.map(cl => {
            if (cl.id === this.videoFocused.id) {
                return this.videoFocused;
            }
            return cl;
        });
        this.videoFocused = undefined;
    }
    @action
    modifyAttributes = (att: string, value: any) => {

        switch (att) {
            case 'name':
                this.videoFocused.name = value;
                break;
            case 'start':
                this.videoFocused.start = value;
                break;
            case 'end':
                this.videoFocused.end = value;
                break;
            default:
                break;
        }
    }
    @action
    openCloseLoading(value: boolean) {
        this.nextVideoLoading = value;
    }
    @action
    allowAutoplay(value: boolean) {
        this.autoplayNext = value;
    }

}

