import * as React from 'react';
// import { Button } from 'reactstrap';
import './reproducer.component.css';
import { Button, Row, Col, Label } from 'reactstrap';
import { Clip } from '../../../../models/clip.model';


class ReproducerProps {

    showControl: boolean;
    setDefault?: any;
    focused?: boolean = false;
    clip: Clip;
    openModal?: any;
    deleteClip?: any;
    showCloseLoading?: any;
    autoNext?: boolean = false;
}

export default class ReproducerComponent extends React.Component<ReproducerProps> {
    constructor(props: ReproducerProps) {
        super(props);
    }

    videoEnded(evt: any) {
        if (((parseInt(evt.currentTime) == this.props.clip.end) || parseInt(evt.currentTime) == 52) && this.props.autoNext) {
            this.props.showCloseLoading(true);
        }

    }

    render() {
        const { clip } = this.props;
        const deleteBtn = this.props.clip.id == 1 ? null : (<Button color="link" onClick={() => this.props.deleteClip(clip.id)}>DELETE</Button>);
        const header = this.props.setDefault ? (
            <Row className="header-row">
                <Col md="12">
                    <Label className="clip-name">Title: {clip.name}</Label>
                </Col>
            </Row>
        ) : null;
        const options = this.props.setDefault ? (
            <Row className="options">
                <Col md="12">
                    <Button color="link" onClick={() => this.props.openModal(clip.id)}>EDIT</Button>
                    <Button color="link" onClick={() => this.props.setDefault(clip.id)}>PLAY</Button>
                    {deleteBtn}
                </Col>
            </Row>

        ) : (
                <Row className="options">
                    <Col md="6">
                        <Label className="clip-name">Title: {clip.name}</Label>
                    </Col>
                    <Col md="3">
                        <Label className="clip-name">Start: {clip.start}</Label>
                    </Col>
                    <Col md="3">
                        <Label className="clip-name">End: {clip.end}</Label>
                    </Col>
                </Row>
            );

        return (
            <Row>
                <Col className="text-center">
                    {header}
                    <Row>
                        <video className="video-reproducer"
                            src={clip.url + '#t=' + clip.start + ',' + clip.end}
                            autoPlay={this.props.focused && this.props.autoNext}
                            controls={this.props.showControl}
                            onPause={(evt) => { this.videoEnded(evt.currentTarget) }}
                        ></video>
                    </Row>
                    {options}
                </Col>
            </Row>
        );
    }
}