import * as React from 'react';
import './add-edit-modal.css';
import { Button, Modal, ModalBody, ModalFooter, Row, Col, Input, Form } from 'reactstrap';
import { Clip } from '../../../../models/clip.model';


class AddEditProps {
    showModal: boolean = false;
    clip?: Clip;
    addClip?: any;
    closeModal: any;
    editClip: any;
    attrModify: any;
}

export default class AddEditVideoModal extends React.Component<AddEditProps, Clip> {
    clip: Clip;
    constructor(props: AddEditProps) {
        super(props);
        this.state = {
            start: 0,
            end: 52,
            name: '',
            id: 0,
            url: ''
        };
    }

    changeStart(evt: any) {
        this.props.attrModify('start', parseInt(evt.target.value))
        this.setState({
            start: parseInt(evt.target.value)
        })
    }
    changeEnd(evt: any) {
        this.props.attrModify('end', parseInt(evt.target.value))
        this.setState({
            end: parseInt(evt.target.value)
        })
    }
    changeName(evt: any) {
        this.props.attrModify('name', evt.target.value)
        this.setState({
            name: evt.target.value
        })
    }
    validateForm() {
        return this.state.name != ''
            && this.state.start >= 0 && this.state.start < this.state.end
            && this.state.end <= 52;
    }
    static getDerivedStateFromProps(nextProps: any, prevState: any) {
        return nextProps.clip;
    }
    render() {
        let { clip, showModal } = this.props;
        let form = clip ? (
            <Form>
                <Row className="row-input">
                    <Col className="text-center">
                        {clip ? 'Edit Clip' : 'Add Clip'}
                    </Col>
                </Row>
                <Row className="row-input">
                    <Col >
                        <Input
                            type="text"
                            placeholder="Name of clip"
                            value={this.state.name}
                            onChange={evt => this.changeName(evt)} />
                    </Col>
                </Row>
                <Row className="row-input">
                    <Col >
                        <Input
                            type="number"
                            placeholder="Start of clip in seconds"
                            min={0}
                            value={this.state.start}
                            onChange={evt => this.changeStart(evt)} />
                    </Col>
                </Row>
                <Row className="row-input">
                    <Col >
                        <Input
                            type="number"
                            placeholder="End of clip in seconds"
                            max={52}
                            value={this.state.end}
                            onChange={evt => this.changeEnd(evt)} />
                    </Col>
                </Row>
                <ModalFooter>
                    <Button color="secondary" onClick={() => {
                        this.props.closeModal();
                    }}>Cancel</Button>{' '}
                    <Button color="success" onClick={() => {
                        if (this.validateForm()) {
                            if (clip.id != 0) {
                                this.props.editClip();
                            } else {
                                this.props.addClip();
                            }
                        }
                    }}> {(clip && clip.id) != 0 ? 'Save Clip' : 'Add Clip'}</Button>
                </ModalFooter>
            </Form>
        ) : null;
        return (
            <div>
                <Modal isOpen={showModal} >
                    <ModalBody>
                        {form}
                    </ModalBody>
                </Modal>
            </div>
        );
    }
}