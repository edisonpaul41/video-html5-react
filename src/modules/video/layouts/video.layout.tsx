import * as React from 'react';
import './video.layout.css';
import Navigation from '../../shared/components/navbar/navbar';
import VideoContainer from '../containers/video/video.container';
import { Route } from 'react-router';
import VideoStore from '../store';
import { Provider } from 'mobx-react';

class VideoLayout extends React.Component {
    applicationStore = new VideoStore();

    render() {
        return (
            <div className="layout">
                <Navigation />
                <Provider store={this.applicationStore}>
                    <Route path="/" component={VideoContainer} />
                </Provider>
            </div>
        );
    }
}

export default VideoLayout;