import * as React from 'react';
import './home.layout.css';
import Navigation from '../../shared/components/navbar/navbar';
import { Route } from 'react-router';
import HomeContainer from '../containers/home/home.container';
class HomeLayout extends React.Component {
    render() {
        return (
            <div>
                <Navigation/>
                 <Route path="/home" component={HomeContainer} />
            </div>
        );
    }
}

export default HomeLayout;