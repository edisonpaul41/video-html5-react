import * as React from 'react';
import logo from '../../../../logo.svg';

class HomeContainer extends React.Component {
    render() {
        return (
            <div>
                 <header className="App-header">
                    <img src={logo} className="app-logo" alt="logo" />
                    <h1 className="App-title">Welcome to my App</h1>
                </header>
            </div>
        );
    }
}

export default HomeContainer;